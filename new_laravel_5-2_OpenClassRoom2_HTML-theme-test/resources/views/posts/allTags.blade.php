@extends('template')

@section('header')
    @if(Auth::check())
        <div class="btn-group pull-right">
			{!! link_to_route('post.create', 'Créer un article', [], ['class' => 'btn btn-info']) !!}
			{!! link_to('logout', 'Deconnexion', ['class' => 'btn btn-warning']) !!}
                        {!! link_to_route('allTag.index', 'Tout les Tags', [], ['class' => 'btn btn-info']) !!}
		</div>
	@else
		{!! link_to('login', 'Se connecter', ['class' => 'btn btn-info pull-right']) !!}
	@endif
@endsection



	@foreach($tags as $post)
		<article class="row bg-primary">
			<div class="col-md-12">
				<header>
					<h1>{{ $post->titre }}
						<div class="pull-right">
							@foreach($post->tags as $tag)
								{!! link_to('post/tag/' . $tag->tag_url, $tag->tag,	['class' => 'btn btn-xs btn-info']) !!}
							@endforeach
						</div>
					</h1>
				</header>
				<hr>
				<section>
				
					<em class="pull-right">
						<span class="glyphicon glyphicon-pencil"></span> {{ $post->user->name }} le {!! $post->created_at->format('d-m-Y') !!}
					</em>
				</section>
			</div>
		</article>
		<br>
	@endforeach



@section('contenu')
	@if(isset($info))
		<div class="row alert alert-info">{{ $info }}</div>
	@endif
        

@endsection