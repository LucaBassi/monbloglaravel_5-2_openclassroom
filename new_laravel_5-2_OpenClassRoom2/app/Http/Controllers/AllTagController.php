<?php

namespace App\Http\Controllers;

use app\Http\Request;

use DB;

use app\Http\Requests;

use App\Http\Controllers\Controller;


class AllTagController extends Controller {

    public function index()
    {

        $tags = DB::select('select * from tags');
      
        return view('posts.stud_view',['tags'=>$tags]);
       
    }
}