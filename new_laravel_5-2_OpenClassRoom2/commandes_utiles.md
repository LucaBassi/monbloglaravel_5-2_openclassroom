## Commandes Composer, Artisan, Etc...
***


####Composer 
composer update
composer install --ignore-platform-reqs
composer dumpautoload
.\vendor\bin\upgrade-carbon


####Artisan
php artisan migrate:install
php artisan migrate:refresh
php artisan migrate:reset

php artisan make:migration create_posts_table
php artisan make:migration create_password_resets_table

php artisan migrate
php artisan clear

php artisan db:seed
php artisan db:seed --class=UserTableSeeder
php artisan db:seed --class=PostTableSeeder

php artisan make:auth

php artisan tinker

php artisan make:middleware Admin
php artisan make:request PostRequest
